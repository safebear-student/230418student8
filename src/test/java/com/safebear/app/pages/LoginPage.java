package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

    private WebDriver driver;

    @FindBy(id = "myid")
    private WebElement username_field;

    @FindBy(id = "mypass")
    private WebElement password_field;

    public LoginPage(WebDriver _driver)
    {
        this.driver = _driver;
        PageFactory.initElements(driver,this);
    }

    public boolean checkCorrectPage(){
        return driver.getTitle().startsWith("Sign In");
    }

    public void login(String username, String password){
        this.username_field.sendKeys(username);
        this.password_field.sendKeys(password);
        this.password_field.submit();
    }
}
