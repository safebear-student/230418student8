package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UserPage {

    private WebDriver driver;

    @FindBy(linkText = "Logout")
    private WebElement logoutLink;

    public UserPage(WebDriver _driver)
    {
        this.driver = _driver;
        PageFactory.initElements(driver,this);
    }

    public boolean checkCorrectPage(){
        return driver.getTitle().startsWith("Logged In");
    }

    public void clickOnLogin()
    {
        logoutLink.click();
    }
}
