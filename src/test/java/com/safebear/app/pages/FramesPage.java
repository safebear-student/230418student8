package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class FramesPage {

    private WebDriver driver;

    public FramesPage(WebDriver _driver)
    {
        this.driver = _driver;
        PageFactory.initElements(driver,this);
    }

    public boolean checkCorrectPage(){
        return driver.getTitle().startsWith("Frame Page");
    }
}
