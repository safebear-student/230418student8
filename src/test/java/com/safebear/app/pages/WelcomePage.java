package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WelcomePage {

    private WebDriver driver;

    @FindBy(linkText = "Login")
    private WebElement loginLink;

    public WelcomePage(WebDriver _driver)
    {
        this.driver = _driver;
        PageFactory.initElements(driver,this);
    }

    public boolean checkCorrectPage(){
        return driver.getTitle().startsWith("Welcome");
    }

    public void clickOnLogin()
    {
        loginLink.click();
    }
}